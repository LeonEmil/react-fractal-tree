# Árbol Fractal

El Árbol de Pitágoras es un plano fractal construido a partir de cuadrados inventado por el profesor Albert E. Bosman en 1942. Lleva el nombre del matemático griego llamado Pitágoras ya que en cada unión de 3 cuadrados se forma un triángulo rectángulo en una configuración tradicional utilizado para representar el teorema de Pitágoras. Si el cuadrado más grande tiene un tamaño de L x L, todo el árbol de Pitágoras encajará perfectamente dentro de una caja del tamaño de 6L × 4L.1​2​ Los detalles más finos de los árboles se asemejan a la curva de Lévy C. 

![Dibujo de un árbol de pitágoras](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Pythagoras_tree_1_1_13_Summer.svg/220px-Pythagoras_tree_1_1_13_Summer.svg.png)

La construcción del árbol de Pitágoras comienza con un cuadrado. Sobre esta plaza se construyen dos cuadrados, cada uno reducido por un factor lineal de ½√2 de tal manera que las esquinas de las plazas coinciden dos a dos. Este mismo procedimiento se aplica de forma recursiva para las dos plazas más pequeñas, hasta el infinito. La siguiente imagen muestra las primeras iteraciones en el proceso de construcción.

![Diagrama que muestra la construcción de arbol de pitágoras en cada iteración](https://res.cloudinary.com/leonemil/image/upload/v1598135583/Demos/diagrama_arbol_de_pitagoras_pnaquh.jpg)

***Ver demostración aqui:***
https://leonemil.gitlab.io/react-fractal-tree/

Cada árbol generado se puede guardar como archivo png.

---

---

# Fractal tree

The Pythagoras tree is a plane fractal constructed from squares. Invented by the Dutch mathematics teacher Albert E. Bosman in 1942, it is named after the ancient Greek mathematician Pythagoras because each triple of touching squares encloses a right triangle, in a configuration traditionally used to depict the Pythagorean theorem. If the largest square has a size of L × L, the entire Pythagoras tree fits snugly inside a box of size 6L × 4L. The finer details of the tree resemble the Lévy C curve. 

![Draw of a pitagoras tree](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Pythagoras_tree_1_1_13_Summer.svg/220px-Pythagoras_tree_1_1_13_Summer.svg.png)

The construction of the Pythagoras tree begins with a square. Upon this square are constructed two squares, each scaled down by a linear factor of √2/2, such that the corners of the squares coincide pairwise. The same procedure is then applied recursively to the two smaller squares, ad infinitum. The illustration below shows the first few iterations in the construction process.

![Diagram of the process of construction of pitagoras tree in each iteration](https://res.cloudinary.com/leonemil/image/upload/v1598135583/Demos/diagrama_arbol_de_pitagoras_pnaquh.jpg)

***See the demo here:***
https://leonemil.gitlab.io/react-fractal-tree/

 Each tree generated can be saved as png file.

---

Source: https://en.wikipedia.org/wiki/Pythagoras_tree_%28fractal%29

