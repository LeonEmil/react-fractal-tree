window.addEventListener("load", () => {

    const canvas = document.getElementById("canvas")
    const button = document.getElementById("button")
    
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    
    let c = canvas.getContext("2d")
    
    let curve
    
    function drawTree(startX, startY, len, angle, branchWidth, color1, color2) {
        c.beginPath()
        c.save()
        c.strokeStyle = color1
        c.fillStyle = color2
        c.shadowBlur = 5
        c.shadowColor = `black`
        c.lineWidth = branchWidth
        c.translate(startX, startY)
        c.rotate(angle * Math.PI / 180)
        c.moveTo(0, 0)
        //c.lineTo(0, -len)
        if (angle > 0) {
            c.bezierCurveTo(20, -len / 2, 20, -len / 2, 0, -len)
        }
        else {
            c.bezierCurveTo(20, -len / 2, -20, -len / 2, 0, -len)
        }
        c.stroke()
    
        if (len < 5) {
            // Leafs
            c.beginPath()
            c.arc(0, -len, 10, 0, Math.PI / 2)
            c.fill()
            c.restore()
            return;
        }
        curve = (Math.random() * 10) + 10
    
        drawTree(0, -len, len * 0.75, angle + curve, branchWidth * 0.5)
        drawTree(0, -len, len * 0.75, angle - curve, branchWidth * 0.5)
    
        c.restore()
    }
    
    drawTree(canvas.width / 2, canvas.height - 80, 120, 0, 2, "green", "green")
    
    const generateRandomTree = () => {
        c.clearRect(0, 0, canvas.width, canvas.height)
        // startX, startY, len, angle, branchWidth, color1, color2
        let centerPointX = canvas.width / 2
        let len = Math.floor((Math.random() * 20) + 100)
        let angle = 0
        let branchWidth = (Math.random() * 70) + 1
        let color1 = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`
        let color2 = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`
        drawTree(centerPointX, canvas.height - 80, len, angle, branchWidth, color1, color2)
    }
    
    button.addEventListener("click", generateRandomTree)
})
