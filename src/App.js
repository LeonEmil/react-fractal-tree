import React from 'react';
import FractalTree from './components/FractalTree'
import Button from './components/Button'

import { Helmet } from 'react-helmet'
import './js/canvas.js'
import './App.css';

function App() {
  const favicon = "https://www.bing.com/th?id=AMMS_bfef14cf3c671eb3aba526b96fddc953&w=110&h=110&c=7&rs=1&qlt=80&cdv=1&pid=16.1"
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Fractal tree</title>
        <link rel="icon" href={favicon} type="image/x-icon"/>
      </Helmet>
      <FractalTree />
      <Button />
    </>
  );
}

export default App;
